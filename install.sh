#!/bin/bash

# EFFECTS:  Set up symlinks to the files in this folder. Installs a terminfo
#           that supports italics.

source "`dirname $0`/global_constants.sh"

FILES="
    bashrc
    bash_aliases
    bash_profile
"

for file in $FILES; do
    if [ -f ~/.$file ]; then
        rm ~/.$file
    fi
    ln -s "${DIR}/${file}" ~/.${file}
done

$DIR/tmux_italics_setup.sh

source ~/.bashrc
