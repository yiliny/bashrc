### Add ~/bin to PATH ###
. ~/.bashrc
setterm blength 0
. "$HOME/.cargo/env"
export PATH="$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin:$PATH"
export PATH="$PATH:/home/$USER/.local/bin:/home/$USER/bin:/home/$USER/.luarocks/bin"
export PATH=$PATH:~/bin
export PATH=/opt/amdgpu-pro/bin:/opt/amdgpu/bin:$PATH
