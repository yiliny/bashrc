[/]
list=['b1dcc9dd-5262-4d8d-a863-c897e6d979b9']

[:b1dcc9dd-5262-4d8d-a863-c897e6d979b9]
foreground-color='rgb(213,213,223)'
visible-name='solarized-yilin'
login-shell=false
palette=['rgb(0,0,0)', 'rgb(138,121,121)', 'rgb(142,166,142)', 'rgb(137,137,120)', 'rgb(140,140,186)', 'rgb(120,56,120)', 'rgb(107,122,122)', 'rgb(187,187,187)', 'rgb(38,38,38)', 'rgb(184,122,122)', 'rgb(143,215,143)', 'rgb(184,184,122)', 'rgb(171,171,234)', 'rgb(175,122,175)', 'rgb(122,184,184)', 'rgb(217,217,217)']
cursor-shape='block'
use-custom-command=false
use-theme-colors=false
use-system-font=false
use-transparent-background=true
font='Source Code Pro 10'
use-theme-transparency=false
background-color='rgb(17,27,46)'
background-transparency-percent=6
audible-bell=true
