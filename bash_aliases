################################################################################
#                              TABLE OF CONTENTS
#===============================================================================
#   General Aliases                                         [GENERAL]
#   git aliases                                             [GIT]
#   vim aliases                                             [VIM]
#   Useful Functions                                        [USEFUL_FUNCTIONS]
#   Functions for MAAV Stuff                                [MAAV_STUFF]
#   Functions for WSL                                       [WSL]
#   asciinema                                               [ASCIICAST]
#===============================================================================

#===============================================================================
#   General Aliases                                         [GENERAL]
#===============================================================================
alias sl="ls"
alias LS="ls"

alias resource='source ~/.bashrc'

alias adsh='eval `ssh-agent -s` && ssh-add'

#===============================================================================
#   git aliases                                             [GIT]
#===============================================================================

# The following are taken from: https://stackoverflow.com/a/4822890
# The commit-ish is obtained by hashing /dev/null.
alias gitcount='git diff --shortstat 4b825dc642cb6eb9a060e54bf8d69288fbee4904'
alias gitcountlong='git diff --stat 4b825dc642cb6eb9a060e54bf8d69288fbee4904'

# EFFECTS:  Append the given entry onto the end of .gitignore.
gitignore()
{
    echo "$1" >> .gitignore
}

# EFFECTS:  Pretty-prints the history differences between two git branches.
# PARAM $1: The first branch.
# PARAM $2: The second branch.
gitBranchDiff()
{
    git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%Creset' --abbrev-commit --date=relative "$1".."$2"
}

#===============================================================================
#   vim aliases                                             [VIM]
#===============================================================================
# EFFECTS:  Open files in a vertical split.
alias nvimv="nvim -O"


#===============================================================================
#   Useful Functions                                        [USEFUL_FUNCTIONS]
#===============================================================================

# EFFECTS:  Create a directory and `cd` into it.
mkcd()
{
    mkdir -p "$@" && cd "$@";
}

clone482()
{
    local params=("${@}")
    git clone "git@github.com:eecs482/$1" "${params[@]:1}"
}

# EFFECTS:  Clone a repository from my bitbucket.
# PARAM:    The repository to clone, with file extension .git
cloneFromYilin()
{
    local params=("${@}")
    git clone "ssh://git@bitbucket.org/yiliny/$1" "${params[@]:1}"
}

alias git-clone-yiliny="cloneFromYilin"

# EFFECTS:  Recursively deletes all .orig, .merge, and .cpp~ and .hpp~
#           files from the current directory down.
deleteMergeJunk()
{
    find . -name "*orig" -exec rm {} +
    find . -name ".merge*" -exec rm {} +
    find . -name "*[ch]pp~*" -exec rm {} +
}

alias clear-junk="deleteMergeJunk"

# EFFECTS:  Recursively deletes all .swp files from the current directory down.
deleteSwapFiles()
{
    find . -name "*swp" -exec rm {} +
}

alias clear-swap="deleteSwapFiles"

# EFFECTS:  Recursively list all .swp files from the current directory down.
listSwapFiles()
{
    find . -name "*swp"
}

alias list-swap="listSwapFiles"

# EFFECTS:  Recursively find all files with names matching the given search
#           pattern, then apply the given perl expression.
# PARAM $1: The filename search pattern.
# PARAM $2: The perl substitution expression.
findSed()
{
    find -name "$1" -exec sed -i "$2" {} \+
}

# EFFECTS:  Runs the given command. All output, including stderr, prints to the
#           screen as usual AND is redirected into a log file.
# PARAM $1: The command to run.
# PARAM $2: The output file.
# DETAIL:   Double-`tee` taken from:
#               https://stackoverflow.com/a/692407
#           Color-removal `sed` command taken from:
#               http://www.commandlinefu.com/commands/view/12043/
#
# REQUIRES: `sudo apt install expect`, to allow for use of `unbuffer`.
teeLog()
{
    COMMAND=$1
    OUTFILE=$2

    > $OUTFILE && unbuffer $COMMAND > >(tee -a $OUTFILE) 2> >(tee -a $OUTFILE >&2)
    sed -i "s,\x1B\[[0-9;]*[a-zA-Z],,g" $OUTFILE  # strip color codes from outfile
}

alias sdloc="export DISPLAY=0"              # use the machine's X server
alias sdrem="export DISPLAY=localhost:10.0" # forward to a remote X server

alias sdget="echo $DISPLAY"             # identify the current X display

#===============================================================================
#   Functions for MAAV Stuff                                [MAAV_STUFF]
#===============================================================================

# EFFECTS:      Start zcm-spy-lite, listening for messages of the given type
#               on the given transport.
# PARAM:        The filepath where zcm-spy-lite should find the message types.
# PARAM:        The desired transport.
zcmSpyLite()
{
    zcm-spy-lite -p $1 -u $2
}

# EFFECTS:      Start zcm-spy-lite, listening to the IPC transport.
# PARAM:        The filepath where zcm-spy-lite should find the message types.
zcmSpyIpc()
{
    zcm-spy-lite -p $1 -u "ipc"
    #zcmSpyLite $1 "ipc"
}

# EFFECTS:      Start zcm-spy-lite, listening to the IPC transport in the simulator
#               build folder.
zcmSpyIpcSim()
{
    zcmSpyIpc ~/maav/nav/build/src/sim
}

alias zcm-spy-sim="zcmSpyIpcSim"

zcmSpyUdpGcs()
{
    #zcm-spy-lite -p ~/maav/nav/build -u '"udpm://239.255.76.67:7667?ttl=1"'
    zcmSpyLite ~/maav/nav/build/src/gcs "udpm://239.255.76.67:7667?ttl=1"
}

alias zcm-spy-gcs="zcmSpyUdpGcs()"


#===============================================================================
#   Functions for WSL                                       [WSL]
#===============================================================================

# EFFECTS:  Fix WSL directory permissions in a directory.
# PARAM $1: `maxdepth` of folders to fix. If set to 1, only "top-level"
#           directories in the current folder will be affected. Defaults to
#           an arbitrarily large value.
fixFolderPermissions()
{
    MAXDEPTH=999999
    if [ $# -ge 1 ]; then
        MAXDEPTH=$1
    fi

    find -maxdepth $MAXDEPTH -type d -exec chmod 775 {} \+
}

# Stop creating folders with full RWX access for literally everybody
makeFolder()
{
    mkdir "$1"
    chmod 775 "$1"
}

#===============================================================================
#   asciinema                                               [ASCIICAST]
#===============================================================================

# Alias for the very long command needed to run asciicast2gif.
alias asciicast2gif='docker run --rm -v $PWD:/data asciinema/asciicast2gif'
