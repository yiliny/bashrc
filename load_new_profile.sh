#!/bin/bash

# EFFECTS:  Installs the given gnome-terminal custom profiles.
# PARAM $1: The dconf profile to install. Defaults to
#           `solarized_yilin.profile`.

source "`dirname $0`/global_constants.sh"

if [ $1 ]; then
    INFILE=$1
else
    INFILE="$DIR/solarized_yilin.profile"
fi

dconf load /org/gnome/terminal/legacy/profiles:/ < $INFILE
