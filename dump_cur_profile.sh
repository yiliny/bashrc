#!/bin/bash

# EFFECTS:  Dumps the current gnome-terminal custom profiles to a text file.
# PARAM $1: (optional) The output file. If not specified, defaults to
# `cur_profile.backup`.

source "`dirname $0`/global_constants.sh"

if [ $1 ]; then
    OUTFILE=$1
else
    OUTFILE="$DIR/cur_profile.backup"
fi

dconf dump /org/gnome/terminal/legacy/profiles:/ > $OUTFILE
