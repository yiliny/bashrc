#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
YILINYGIT="git@bitbucket.org:yiliny"
INSTALLCMD="sudo apt-get install -y --fix-missing"

# Start from Home Folder
cd ~/
