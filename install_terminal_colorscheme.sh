#!/bin/bash

# EFFECTS:  - Backs up my current gnome-terminal custom profiles.
#           - Installs my custom colorscheme, usable in GNOME terminal.

source "`dirname $0`/global_constants.sh"

$DIR/dump_cur_profile.sh
$DIR/load_new_profile.sh
