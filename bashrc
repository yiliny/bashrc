# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# Current source folder.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Set terminal type early.
export TERM=xterm-256color

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

################################################################################
# Bash Prompts
################################################################################
#===============================================================================
# Set the primary prompt variable.
# Color Scheme Formatting
#  https://www.cyberciti.biz/faq/bash-shell-change-the-color-of-my-shell-prompt-under-linux-or-unix/
#   Color Scheme Syntax
#     '\e[x;ym <STUFF> \e[m' # without the single quotes
#       \e[     : start color scheme
#       x;y     : color pair to use (x;y)
#       <STUFF> : the stuff you want to color
#       \em     : stop color scheme
#
# General Formatting
#  https://www.howtogeek.com/307701/how-to-customize-and-colorize-your-bash-prompt/
#
# Escape Characters
#  http://tldp.org/HOWTO/Bash-Prompt-HOWTO/bash-prompt-escape-sequences.html
#===============================================================================

# Colors
BLUE='0;34'
GREEN='0;32'
YELLOW='0;33'
CYAN='0;96'
RED='0;31'

# EFFECTS:  Return an integer. Return value grows monotonically with each call.
# PARAM $BASE:      Start incrementing from this value.
# PARAM $ITER_INCR: Increment the global counter by this much.
export TICKER=0
gradualGrowth(){
    BASE=$1
    ITER_INCR=$2

    export TICKER=$[TICKER + ITER_INCR]
    printf "$[BASE + TICKER]"
}

# EFFECTS:  wHAT wAs ShaLl Be wHat SHaLl BE WaS
# PARAM MAX_SIZE: The maximum number of characters with which to populate the output string.
randomZalgo(){
    MAX_SIZE=$1

    ZALGO=("#̴̵̶̷̸̯̰̱̲̳̹̺̻̼͇͈͉̽̾̿̀́͂̓̈́͆͊͋ͅ", "&̡̢̧̨̛̖̗̘̙̜̝̞̟̠̣̤̥̦̩̪̫̬̭̮̀́̂̃̄̅̆̇̈̉̊̋̌̍̎̏̐̑̒̓̔̕̚", "!̡̢̧̨̛̖̗̘̙̜̝̞̟̠̣̤̥̦̩̪̫̬̭̮̀́̂̃̄̅̆̇̈̉̊̋̌̍̎̏̐̑̒̓̔̕̚", "A͍͎͌͏͓͔͕͖͙͚͐͑͒͗͛ͣͤͥͦͧͨͩͪͫͬͭͮͯ͘͜͟͢͝͞͠͡", "*̴̵̶̷̸̯̰̱̲̳̹̺̻̼͇͈͉̽̾̿̀́͂̓̈́͆͊͋ͅ")             # possible Zalgo characters
    let ZALGO_SIZE=${#ZALGO[@]}
    ZALGO_STR=""

    let NUM_VALS=$[ $RANDOM % $MAX_SIZE + 1]
    for v in $(seq 1 $NUM_VALS); do
        let INDEX=($[ $RANDOM % $ZALGO_SIZE])   # generate a random, valid index
        ZALGO_STR+=${ZALGO[$INDEX]}             # append to the string
    done
    printf $ZALGO_STR
}


# Convenience Variables
DEBIAN_CHROOT='${debian_chroot:+($debian_chroot)}'

#===============================================================================
# Primary Bash Prompt
#===============================================================================
if [ "$color_prompt" = yes ]; then
    if [ "$WGAHNAGL" = yes ]; then
        #PS1="$DEBIAN_CHROOT\[\e[01;32m\]\u@\h\[\e[00m\]:\[\e[01;34m\]\w\[\e[00m\]\$ "
        PS1="$DEBIAN_CHROOT\[\e[01;32m\]\u"
        PS1+='$(randomZalgo $(gradualGrowth 0 10) )'
        PS1+="@\h\[\e[00m\]"
        PS1+='$(randomZalgo $(gradualGrowth 0 10) )'
        PS1+=":\[\e[01;34m\]\w\[\e[00m\]\$ "
        PS1+='$(randomZalgo $(gradualGrowth 10 10 ) )'
    else
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
              # ^ tell debian that I'm using a debian_chroot environment
                          # ^ \[ <STUFF> \] : begin a sequence of non-printing characters
                                                 # ^ ASCII escape character, octal \034
    fi
    # I literally just asked ChatGPT to escape my PS1 properly and it did.
    # I, for one, welcome our new AI overlords.
    # PS1="\[\e[1;32m[\u@\h\[\e[m \[\e[1;36m\D{%H:%M:%S}\[\e[m \[\e[1;35m\W\[\e[m\[\e[1;32m]\$ \[\e[m"
    PS1="\[\e[1;32m\][\u@\h]\[\e[m\] \[\e[1;36m\]\D{%H:%M:%S}\[\e[m\] \[\e[1;35m\]\W\[\e[m\]\[\e[1;32m\]\$ \[\e[m\]"
else
    PS1="$DEBIAN_CHROOT\u@\h:\w\$ "
fi

#===============================================================================
# Secondary Bash Prompt
#===============================================================================
PS2="   : "
#===============================================================================


unset color_prompt force_color_prompt

#===============================================================================
################################################################################
# End Bash Prompts
################################################################################


# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias ls='ls --color'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi
# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Source alacritty bash completion scripts.
if [ -f $DIR/alacritty-completions.bash ]; then
    source $DIR/alacritty-completions.bash
fi

# Set neovim as the preferred text editor.
export VISUAL=nvim
export EDITOR="$VISUAL"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
