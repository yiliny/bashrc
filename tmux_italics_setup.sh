#!/bin/bash

# EFFECTS:  Makes italics work when launching vim in a tmux window.

source "`dirname $0`/global_constants.sh"

tic $DIR/screen-256color-richtext.terminfo
